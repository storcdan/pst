# Poznámky pro předmět PST
## Přednášky
1. [Úvod](lectures/01.md)
1. [Pravděpodobností prostory](lectures/02.md)
1. přednáška
...

## Cvičení
Playlist na youtube od doktora Novotného [zde](https://www.youtube.com/playlist?list=PLRibGI2QMergzhqk8U-EraWOnBLZthZh5)

1. cvičení
    * [zadání](labs/01/cviko1.pdf)
    * [vypracované](labs/01/solution.md)
    * [video](https://youtu.be/umuKJZSiUUQ)
1. cvičení
