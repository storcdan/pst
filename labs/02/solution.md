# Cvičení 22.2.2021

## G1 

Ukažte jestli $`P:\mathcal{A}\rightarrow \mathbb{R}`$ definuje pravděpodobnost na $`\Omega`$, je li dáno:

1. $`\Omega = \{1, 2, 3\};\:\mathcal{A} = 2^\Omega;\:P(A) = \frac{1}{3}|A|;\: A\in\mathcal{A}`$
    1. $`P(\Omega) = \frac{|\Omega|}{3} = \frac{3}{3} = 1`$ - splňuje
    2. $`P(\emptyset) = \frac{|\emptyset|}{3} = \frac{0}{3} = 0`$ - splňuje
    3. $`A\in\mathcal{A}\;P(A)\geq 0`$ - splňuje
    4. $`P(A\bigcup B) = \frac{|A\bigcup B)|}{3} = \frac{|A|}{3} + \frac{|B)|}{3} = P(A) + P(B)`$ - splňuje
1. $`\Omega = \{1, 2, 3\};\:\mathcal{A} = 2^\Omega;\:P(A) = 1;\: 1\notin A`$ a  $`P(A) = 0;\:1\in A`$
    1. $`P(\Omega) = 3`$ - nesplňuje 
1. $`\Omega = \mathbb{N};\:\mathcal{A} = 2^\Omega;\:P(A) = \sum_{i\in A}2^{-i}`$
    1. $`P(\Omega) = \sum_{i\in \mathbb{N}}2^{-i} = \sum_{i=1}^{\infty}2^{-i} = 1`$ - splňuje
    2. $`P(\emptyset) = \sum_{i\in \mathbb{N}}2^{-i} = 0`$ - splňuje, nic se nesčítá dohromady
    3. $`A\in\mathcal{A}\;P(A)\geq 0`$ - splňuje
    4. $`P(A\bigcup B) = \sum_{i\in A\bigcup B}2^{-i} = \sum_{i\in A}2^{-i} + \sum_{i\in B}2^{-i} = P(A) + P(B)`$ - splňuje
1. $`\Omega = \mathbb{R};\:\mathcal{A} = 2^\Omega;\:P(A) = 1;\: 0\in A`$ a  $`P(A) = 0;\:0\notin A`$
    1. $`P(\Omega) = 1`$ - splňuje
    2. $`P(\emptyset) = 0`$ - splňuje
    3. $`A\in\mathcal{A}\;P(A)\geq 0`$ - splňuje
    4. $`P(A\bigcup B) = P(A) + P(B)`$ - splňuje

## G2
Hodíme dvěma kostkami.
1. Jaká je pravděpodobnost, že bude součin liché číslo?
```math
P = \frac{1}{4}
```
2. Jaká je pravděpodobnost, že bude součet roven sedmi?
```math
P = \frac{12}{36} = \frac{1}{6}
```
3. Pravděpodobnostní prostor, elementarní jevy, jevy označující příklady 1. a 2.
    1. Pravděpodobnostní prostor
    ```math
    \Omega = \{1, 2, 3, 4, 5, 6\}^2\\
    \mathcal{A} = 2^\Omega\\
    P(A) = \frac{|A|}{|\Omega|} = \frac{|A|}{36}
    ```
    2. Elementaární jevy
    ```math
    \{1, 1\}, \{1, 2\},\dots, \{1, 6\}, \{2, 1\},\dots,\{6, 6\}
    ```
    3. Jevy příkladu 1. jsou páry liché krát liché např. $`\{1, 3\}`$. Takovýchto párů je 9.
    4. Jevy příkladu 2. jsou páry jejich součet dává číslo 7, např. $`\{4, 3\}`$. Takovýchto párů je 6.

## G3
