# Cvičení 15.2.2021

## G1
Přehrávač v náhodném pořadí přehraje písně A,B,C,D,E (všechna pořadí jsou stejně pravděpodobná).
Jaká je pravděpodobnost, že
1. Je přehraje v pořadí ABCDE?
```math
P(ABCDE) = \frac{1}{5!} = \frac{1}{120} 
```
2. píseň A bude první a píseň E poslední?
```math
P(A\_\_\_E) = \frac{3!}{5!} = \frac{1}{20}
```
3. píseň A bude hrát bezprostředně před B?
    * Bereme píseň A a B jako jeden prvek, poté je možností jak přehrát písně $`4!`$
```math
P(AB) = \frac{4!}{5!} = \frac{1}{5}
```
4. píseň A bude hrát kdykoliv před B?
```math
P(A < B) = P(B < A) = \frac{1}{2}\\
P(A < B) + P(B < A) = 1
```
5. píseň A bude hrát před B a píseň E před A?
```math
P(A < B < E) = P(B < A < E) = P(E < B < A) = \dots = \frac{1}{3!} = \frac{1}{6}
```
## G2
Ve třídě je 15 chlapců a 10 dívek, kolik 6 člených týmů lze utvořit, pokud

Binomické číslo $`\binom{n}{k} = \frac{n!}{k!(n-k)!}`$
1. nejsou specifikované další podmínky?
```math
num_{teams} = \binom{25}{6} = 177100
```
2. jsou v týmu samí chlapci?
```math
num_{teams} = \binom{15}{6} = 5005
```
3. jsou v týmu právě dvě dívky
```math
num_{teams} = \binom{10}{2}\binom{15}{4} = 61425
```
4. jsou v týmu alespň dvě dívky
```math
num_{teams} = \binom{25}{6} - \binom{10}{1}\binom{15}{5} - \binom{15}{6} = 142065
```

## G3
3 Náhodný binární kód je řetězec znaků 0, 1, kde každý ze znaků má stejnou pravděpodobnost výskytu a
znaky se vyskytují nezávisle. Uvažme náhodný binární kód délky 8. Jaká je pravděpodobnost, že

1. Bude začínat jedničkou?
```math
P(1\_\_\_\_\_\_\_) = \frac{2^7}{2^8} = \frac{1}{2}
```
2. Bude začínat jedničkou a končit nulou?
```math
P(1\_\_\_\_\_\_0) = \frac{2^6}{2^8} = \frac{1}{4}
```
3. Bude obsahovat alespoň 3 nuly?
    * Vím že mám práve tři **0** a 5 **1**. Takže mně zajíma na kolik pozic můžu umístit nuly v osmi znacích
```math
P(3*0) = P(5*1) = \frac{\binom{8}{3}}{2^8} = \frac{\binom{8}{5}}{2^8} = \frac{7}{32}
```
4. Bude obsahovat alespoň 3 nuly
    * Možnost sečít možnosti pro 3, 4, 5... ale to je moc práce. Lepší řešení odečíst opak od 1
```math
P(>=3*0) = 1 - P(<3*0) = 1 - \frac{\binom{8}{2}}{2^8} - \frac{\binom{8}{1}}{2^8} - \frac{\binom{8}{0}}{2^8} = \frac{219}{256}
```
5. Bude začínat jedničkou a bude obsahovat právě tři nuly
```math
P = \frac{\binom{7}{3}}{2^8} = \frac{35}{256}
```
6. bude začínat jedničkou, končit nulou a obsahovat alespoň tři nuly?
```math
P = \frac{2^6 - \binom{6}{1} - 1}{2^8} = \frac{57}{256}
```


## G4
Házíme mincí, u které je pravděpodobnost hodu panny rovna $`p > 0`$
1. Jaká je pravděbodobnost že ve třech hodech padne *panna, panna, orel* v tomto pořadí?
```math
P(PPO) = pp(1-p) = p^2(1-p)
```
2. Jaká je pravděpodobnost že padne panna dvakrát ze čtyř hodů?
    * **Nezapomenout vynásobit počtem kobinací !!**
```math
P = p^2(1-p)^2\binom{4}{2}
```
3. Hazím mincí, dokud nepadne panna. Jáká je pravděpodobnost že jsem házel více než třikrát?
    * Několik přístupů, nejpříjemnější pravděpodobnost že panna nepadne v prvních 3 kolech
    * Jiné přístupy
        * Součet nekonečné řady
        * Odčítat opak od jedničky
```math
P = (1-p)^3
``` 


## G5
Ruská ruleta. V bubínku revolveru je 6 míst a právě dva po sobě jdoucí náboje. Zatočíme bubínkem,
vystřelíme do vzduchu a vidíme, že jsme vystřelili naprázdno. Pokud máme nyní přiložit revolver k hlavě a
vystřelit, je výhodnější předtím znovu bubínkem zatočit nebo ne? Jaké jsou příslušné pravděpodobnosti, že
vystřelíme náboj?

Šance na náboj po protočení
```math
P = \frac{2}{6} = 33.3\%
``` 
Šance na náboj po ponechání
```math
P = \frac{1}{4} = 25.0\%
``` 

Není výhodnější bubínkem zatočit

## G6
Hypergeometrické rozdělení. Máme M sirek, z nichž právě K nemá hlavičku. Jaká je pravděpodobnost,
že při slepém tahu m sirek bude právě k bez hlavičky?

* Počet možných zbůsobů tahů sirek $`\binom{M}{m}`$
```math
P = \frac{\binom{K}{k}\binom{M-K}{m-k}}{\binom{M}{m}}
``` 

